import boto3
import logging
import os
from botocore.exceptions import ClientError

log_format = "%(asctime)s %(levelname)s %(message)s"
logging.basicConfig(level="INFO", format=log_format, datefmt="%Y-%m-%d %H:%M:%S")

S3_BUCKET_NAME = os.getenv('S3_BUCKET_NAME')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3 = boto3.resource('s3')

my_bucket = s3.Bucket(S3_BUCKET_NAME)

try:
    for my_bucket_object in my_bucket.objects.all():
        print(my_bucket_object.key)

except ClientError as e:
    if e.response['Error']['Code'] == 'NoSuchBucket':
        logger.error(f"Bucket names {S3_BUCKET_NAME} not found")
    elif e.response['Error']['Code'] == 'SignatureDoesNotMatch':
        logger.error("Authorization problem. Please, check you AWS credentials")
    else:
        logger.error(e)

