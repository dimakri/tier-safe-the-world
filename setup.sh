#!/bin/bash

# --- Setting env variables -------------------------------------------
export AWS_ACCESS_KEY_ID=XXXXXXXXXXXXXXXXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXX
export AWS_DEFAULT_REGION="eu-central-1"
export AWS_REGION="eu-central-1"
export S3_BUCKET_NAME="tier-safe-the-world"

cd ./tf
# --- Running Terraform -------------------------------------------
terraform init
terraform plan
terraform apply -auto-approve
