

 <br />
 <p align="center">
   <h3 align="center">README</h3>

   <p align="center">
     TIER take-home exercise
   </p>
 </p>

 <details open="open">
   <summary>Table of Contents</summary>
   <ol>
     <li>
       <a href="#about-the-project">About The Project</a>
     </li>
     <li>
       <a href="#getting-started">Getting Started</a>
       <ul>
         <li><a href="#directory-structure">Directory Structure</a></li>
         <li><a href="#prerequisites">Prerequisites</a></li>
         <li><a href="#usage">Usage</a></li>
       </ul>
     </li>
     <li><a href="#contact">Contact</a></li>
   </ol>
 </details>


 ## About The Project

 ### The assigment
- Create an object store in cloud provider of choice by terraform (eg. S3 in AWS)
- Upload something
- Create a small tool in language of choice to enumerate the object store
- Instrument the application so that it emits relevant metrics and logs. Consider how you would observe the behavior of this application and what an appropriate set of SLIs and SLOs could be and provide a brief explanation to your reasoning in a README file
- Create a Dockerfile to host the tool
- Bonus: deploy to kubernetes (eg. minikube) with tooling of choice
- Bonus: roughly describe possible credential storage

 ### Directory Structure
```sh
.
├── BLOBS
│   ├── IMG_4548-1.jpeg
│   ├── cinnamon-raisin-sourdough-bread-4.jpg
│   ├── d.jpg
│   ├── images.jpg
│   ├── imagesd.jpg
│   ├── imagesdd.jpg
│   ├── small-batch-sourdough-bread-8.jpg
│   ├── theperfectloaf-beginners-sourdough-bread-v-1-1.jpg
│   └── theperfectloaf-no-knead-sourdough-bread-1.jpg
├── Dockerfile
├── README.md
├── main.py
├── requirements.txt
├── setup.sh
└── tf
    └── main.tf
```
 ## Getting Started
 ### Prerequisites
 Following software needs to be installed on you computer:
 - python 3 ( to run the app localy)
 -  docker
 - terraform 1.0.0
### Description

 Since this is basically a python script that is executed by hand and once executed it outputs a list of files in an S3 bucket. This service is dependant on S3     availability (stated SLA 99.999999999%). If I was to write this application as a web service (a webpage or an API Endpoint that return a list of files) I would  start with these basic SLIs:

 - App's availability
 - Response time
 - Average response time

 As for SLOs (as a raw example):
 - 99% of requests to our service should take less than 300 ms
 - average response time < 250 ms
 - app's availability should be 99.99%


 This particular iteration of the app works as a simple command-line tool with minimal error handling (for now it checks if AWS credentials are correct and if the S3 bucket exists). Ask for a potential credential storage solution, I would use Vault to store AWS_ACCESS_KEY_ID, and AWS_SECRET_ACCESS_KEY and access this pair from code using another API key. If this app is to be written as a web service then I would deploy it into a Kubernetes cluster using a GitOps tool like  Fluxcd or ArgoCD, encrypt
 the credentials using [sealed-secrets](https://github.com/bitnami-labs/sealed-secrets) and deploy these credentials as Kubernetes secrets using a helm chart.

### Usage


1. Open setup.sh and a provide your AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY:
2. Run setup.sh

    ```sh
    ./setup.sh
    ```

    This will create create a *tier-safe-the-world* bucket using terraform and upload BLOBs from the ./BLOB directory  to that bucket ( just a handfull of sourdough bread images)
    
 3. Install all of the requirements:

 ```sh
    pip3 install -r ./requirements.txt
 ```

 4. Set environmet variable S3_BUCKET_NAME:

```sh
 export S3_BUCKET_NAME=tier-safe-the-world
```

5. Set your AWS credentials using *aws configure*.
6. Execute main.py:

```sh
python3 main.py
```

  Output:

   ```sh
     IMG_4548-1.jpeg
	cinnamon-raisin-sourdough-bread-4.jpg
	d.jpg
	images.jpg
	imagesd.jpg
	imagesdd.jpg
	small-batch-sourdough-bread-8.jpg
	theperfectloaf-beginners-sourdough-bread-v-1-1.jpg
	theperfectloaf-no-knead-sourdough-bread-1.jpg
 ```

To run this app using docker do the following:

1. Build the docker container:
```sh
docker build -t tier-app .
```
2. Run the container replacing XXXXXXXX with you AWS credeentials:

```sh
docker run -e AWS_ACCESS_KEY_ID=XXXXXXXXXXX -e AWS_SECRET_ACCESS_KEY=XXXXXXXXXXX  -e S3_BUCKET_NAME=tier-safe-the-world tier-app:latest
```
output:

```sh
2021-10-04 17:15:14 INFO Found credentials in environment variables.
IMG_4548-1.jpeg
cinnamon-raisin-sourdough-bread-4.jpg
d.jpg
images.jpg
imagesd.jpg
imagesdd.jpg
small-batch-sourdough-bread-8.jpg
theperfectloaf-beginners-sourdough-bread-v-1-1.jpg
theperfectloaf-no-knead-sourdough-bread-1.jpg
```
 ## Contact

 Dmitrii Krivosheev - dkrivosheyev@gmail.com

