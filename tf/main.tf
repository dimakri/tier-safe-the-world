terraform {
  required_providers {
    aws = {
      version = ">= 3.40.0"
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

resource "aws_s3_bucket" "tier_blob_storage" {
  bucket = "tier-safe-the-world"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}


resource "aws_s3_bucket_object" "dist" {
  for_each = fileset("../BLOBS/", "*")

  bucket = aws_s3_bucket.tier_blob_storage.id
  key    = each.value
  source = "../BLOBS/${each.value}"
  etag   = filemd5("../BLOBS/${each.value}")
}



